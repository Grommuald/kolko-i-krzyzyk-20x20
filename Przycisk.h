#pragma once
#include "Funkcje.h"
#include "Tekst.h"

class Przycisk
{
public:
	Przycisk(string Tekst, int x, int y, bool odznacz = false ,bool ZostawPodswietlenie = false)
	{
		pozycja.x= x;
		pozycja.y= y;

	
		TekstPrzycisku = Tekst;

		wcisniety = false;
		podswietlony = false;

		zostawPodswietlenie = ZostawPodswietlenie;

		
		przyciskOdznaczajacy = odznacz;

		if(!odznacz)
		{
	     	pozycja.w = wycinek.w = 80;
		    pozycja.h = wycinek.h = 32;
		}
		else
		{
			pozycja.w = wycinek.w = 32;
			pozycja.h = wycinek.h = 32;
		}


	}
	 Przycisk();
	~Przycisk(){}

	bool ZwrocWcisniecie(){return wcisniety;}
	void AktualizujWejscie(SDL_Event kontrola);
	void RysujPrzycisk(SDL_Surface *obraz, SDL_Surface *gdzie, int r, int g, int b);
	


private:

	bool wcisniety;    //gdy wcisniemy przycisk ustawia true
	bool podswietlony; //gdy najedziemy na przycisk kursorem, to si� pod�wietla

	bool zostawPodswietlenie; //po wcisnieciu zostawiamy podswietlenie
	bool przyciskOdznaczajacy;

	string TekstPrzycisku;

	SDL_Rect pozycja;
	SDL_Rect wycinek;

};