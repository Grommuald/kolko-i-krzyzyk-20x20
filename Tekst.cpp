#include "Tekst.h"


Tekst::Tekst()
{
	    x = y = 0;
		r = g = b = 255;
        wielkosc = 0;

        czcionkaKolor.r  = r;
		czcionkaKolor.g  = g;
		czcionkaKolor.b  = b;

		czcionka = NULL;
		nazwaCzcionki = "morgue.ttf";

		czcionkaPow = NULL;

}
Tekst::Tekst(int x, int y, int wielkosc, const char *nazwaCzcionki)
{
	    this->x = x;
		this->y = y;
		this->wielkosc = wielkosc;

		this->nazwaCzcionki = nazwaCzcionki;

		r = 255;
		g = 255;
		b = 255;

        czcionkaKolor.r  = r;
		czcionkaKolor.g  = g;
		czcionkaKolor.b  = b;

		czcionka = NULL;
		czcionkaPow = NULL;


}
Tekst::~Tekst()
{
	SDL_FreeSurface(czcionkaPow);
	TTF_CloseFont(czcionka);
}
void Tekst::ZmienPozycje(int wyznacz_x, int wyznacz_y)
{
		x = wyznacz_x;
		y = wyznacz_y;
}
void Tekst::ZmienKolor(int wyznacz_r, int wyznacz_g, int wyznacz_b)
{
		r = wyznacz_r;
		g = wyznacz_g;
		b = wyznacz_b;

        czcionkaKolor.r  = r;
		czcionkaKolor.g  = g;
		czcionkaKolor.b  = b;

}
void Tekst::ZmienWielkosc(int wyznaczWielkosc)
{
		wielkosc = wyznaczWielkosc;
}


int Tekst::PiszTekst(string tresc, SDL_Surface *gdzie)
{
	czcionka = TTF_OpenFont(nazwaCzcionki, wielkosc); //otwieramy czcionke
	czcionkaPow = TTF_RenderText_Solid(czcionka, tresc.c_str(), czcionkaKolor); //ladujemy do jej powierzchni czcionke
	if(czcionkaPow == NULL)
		return 1;

	Rysuj(x, y, czcionkaPow, gdzie);
	
}

int  Tekst::PiszTekst(int tresc, SDL_Surface *gdzie)
{
	char temp_buff[100]; //tworzymy bufor pomocniczy (do liczb)
	for(int i = 0; i < 100; ++i) temp_buff[i] = 0;

	sprintf(temp_buff, "%d", tresc); //zapisujemy do tablicy wynik naszych danych

	czcionka = TTF_OpenFont(nazwaCzcionki, wielkosc); //otwieramy czcionke
	czcionkaPow = TTF_RenderText_Solid(czcionka, temp_buff, czcionkaKolor); //ladujemy do jej powierzchni czcionke
	if(czcionkaPow == NULL)
		return 1;

	Rysuj(x, y, czcionkaPow, gdzie);


}
int Tekst::PiszTekst(float tresc, SDL_Surface *gdzie)
{
	char temp_buff[100]; //tworzymy bufor pomocniczy (do liczb)
	for(int i = 0; i < 100; ++i) temp_buff[i] = 0;

	sprintf(temp_buff, "%f", tresc); //zapisujemy do tablicy wynik naszych danych

	czcionka = TTF_OpenFont(nazwaCzcionki, wielkosc); //otwieramy czcionke
	czcionkaPow = TTF_RenderText_Solid(czcionka, temp_buff, czcionkaKolor); //ladujemy do jej powierzchni czcionke
		if(czcionkaPow == NULL)
		return 1;

	Rysuj(x, y, czcionkaPow, gdzie);


}
int Tekst::PiszTekst(double tresc, SDL_Surface *gdzie)
{
	char temp_buff[100]; //tworzymy bufor pomocniczy (do liczb)
	for(int i = 0; i < 100; ++i) temp_buff[i] = 0;

	sprintf(temp_buff, "%e", tresc); //zapisujemy do tablicy wynik naszych danych

	czcionka = TTF_OpenFont(nazwaCzcionki, wielkosc); //otwieramy czcionke
	czcionkaPow = TTF_RenderText_Solid(czcionka, temp_buff, czcionkaKolor); //ladujemy do jej powierzchni czcionke
	if(czcionkaPow == NULL)
		return 1;

	Rysuj(x, y, czcionkaPow, gdzie);



}
 