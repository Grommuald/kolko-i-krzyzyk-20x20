#include "Gra.h"


void Gra::AktualizujWejscie(SDL_Event kontrola)
{

  //pozycja myszy
    int x = 0, y = 0; 
   //pobieramy pozycje myszy
   SDL_GetMouseState( &x, &y ); 
   SDL_Rect mysz = {x, y, 1, 1};


if(!koniecImprezy) //gdy wygramy lub jest remis
{
 //podswietlamy pola 
 for(int i = 0; i < MAX_X; ++i)
 {
	for(int j = 0; j < MAX_Y; ++j)
	{
      if(Kolizja(mysz, poleGry[i][j]->ZwrocPole()) == true)
	  {
       poleGry[i][j]->podswietlenie = true;
	   //gdy klikniemy
	     if(poleGry[i][j]->aktywne)
	     {
			 //kontrolujemy mysz
	        if( kontrola.type == SDL_MOUSEBUTTONDOWN ) 
            { 
	          //lewy przycisk
	          if( kontrola.button.button == SDL_BUTTON_LEFT ) 
	          { 
		       poleGry[i][j]->ZmienStanPola(aktualnaTura);
			   poleGry[i][j]->aktywne = false;
			   liczRemis++;
			   ZmienTure();

	          }
			}
		}
	  }
	  else
	  {
	        poleGry[i][j]->podswietlenie = false;
	  }
	}
 }
}

if(pytajIloscGraczy)
{
   koniecImprezy = true;
   if( kontrola.type == SDL_KEYDOWN )
   {
          switch( kontrola.key.keysym.sym )
          {
             case SDLK_t:
                dwochGraczy = false;
				trzechGraczy = true;
				wyswietlPytanie = false;
				LosujGracza();
				koniecImprezy = false;
				pytajIloscGraczy = false;
                break;

			 case SDLK_n:
				dwochGraczy = true;
				trzechGraczy = false;
				wyswietlPytanie = false;
				LosujGracza();
				koniecImprezy = false;
				pytajIloscGraczy = false;
				break;

          }
   }

}
restart->AktualizujWejscie(kontrola);

}

void Gra::RysujWszystko(SDL_Surface *obrazek, SDL_Surface *gdzie)
{
 Tekst iloscGraczy(645, 100, 20, "morgue.ttf");
 Tekst tytul(650, 0, 30, "morgue.ttf");
 Tekst podtytul(750, 35, 25, "morgue.ttf");
 Tekst start(800, 600, 20, "morgue.ttf");
 Tekst gracz(900, 595, 25, "morgue.ttf");

 start.ZmienKolor(10, 10, 255);
 gracz.ZmienKolor(10, 10, 255);
 iloscGraczy.ZmienKolor(255, 10, 10);



 tytul.PiszTekst("Kolko i Krzyzyk v.2.0", gdzie);
 podtytul.PiszTekst("Wersja Epicka", gdzie);
 
 start.PiszTekst("Zaczyna ", gdzie);
 gracz.PiszTekst(ktoZaczyna, gdzie);
 


 UstawKluczowanieKoloru(obrazek, 255, 0, 255);
 //najpierw pole

 if(wyswietlPytanie)
 {
    iloscGraczy.PiszTekst("Wlaczyc tryb dla 3 graczy? T/N", gdzie);

 }
 for(int i = 0; i < MAX_X; ++i)
 {
	for(int j = 0; j < MAX_Y; ++j)
	{
		poleGry[i][j]->RysujPole(obrazek, gdzie);
	}
 }
 restart->RysujPrzycisk(obrazek, gdzie, 10, 255, 10);


}
void Gra::LosujGracza()
{

if(dwochGraczy)
{
	srand(time(NULL));
	aktualnaTura = rand() % 2 + 1;
	if(aktualnaTura == 1)
		ktoZaczyna = "Kolko";
	else if(aktualnaTura == 2)
		ktoZaczyna = "Krzyzyk";
}
else if(trzechGraczy)
{
	srand(time(NULL));
	aktualnaTura = rand() % 3 + 1;
	if(aktualnaTura == 1)
		ktoZaczyna = "Kolko";
	else if(aktualnaTura == 2)
		ktoZaczyna = "Krzyzyk";
	else if(aktualnaTura == 3)
		ktoZaczyna = "Plus";
}


}
void Gra::OdNowa()
{

    pytajIloscGraczy = true;
    wyswietlPytanie = true;
  
    for(int i = 0; i < MAX_X; ++i)
    {
		for(int j = 0; j < MAX_Y; ++j)
		{
			    delete poleGry[i][j];
				poleGry[i][j] = new Pole(0, j*32, i*32);
		}
	}
	

	   wygranaKolko = false;
	   wygranaKrzyzyk = false;
	   wygranaPlus = false;

	   liczRemis = 0;
	   remis = false;
	  
	

}
bool Gra::Wygrana(SDL_Surface *gdzieRysowac)
{

	if(restart->ZwrocWcisniecie() == true)
	{
		OdNowa();
		
	}
	Tekst tekstWygranej(650, 100, 23, "morgue.ttf");
	tekstWygranej.ZmienKolor(255, 10, 10);
	Tekst tekstZwyciezcy(650, 200, 40, "morgue.ttf");

	if(koniecImprezy)
	{
		//usuwamy podswietlenie
		for(int i = 0; i < MAX_X; ++i)
	    {
		   for(int j = 0; j < MAX_Y; ++j)
	       {
		    	poleGry[i][j]->podswietlenie = false;
		   }
	    }
	}
	if(wygranaKolko)
	{

	koniecImprezy = true;
	tekstWygranej.PiszTekst("Wygralo", gdzieRysowac);
	tekstZwyciezcy.PiszTekst("KOLKO!", gdzieRysowac);
	}
	else if(wygranaKrzyzyk)
	{
	koniecImprezy = true;
	tekstWygranej.PiszTekst("Wygral", gdzieRysowac);
	tekstZwyciezcy.PiszTekst("KRZYZYK!", gdzieRysowac);
	}
	else if(wygranaPlus)
	{
	koniecImprezy = true;
	tekstWygranej.PiszTekst("Wygral", gdzieRysowac);
	tekstZwyciezcy.PiszTekst("PLUS!", gdzieRysowac);
	}

	else if(remis)
	{
	koniecImprezy = true;
	tekstWygranej.PiszTekst("Jest", gdzieRysowac);
	tekstZwyciezcy.PiszTekst("REMIS!", gdzieRysowac);
	}

	return true;
	
}

void Gra::SprawdzPole(SDL_Surface *obraz, SDL_Surface *gdzie)
{
	
	for(int i = 0; i < MAX_X; ++i)
	{
		for(int j = 0; j < MAX_Y; ++j)
		{
			//dla poziomu
			if(poleGry[i][j]->ZwrocStanPola() == 1 && poleGry[i][j+1]->ZwrocStanPola() == 1  
				&& poleGry[i][j+2]->ZwrocStanPola() == 1  && poleGry[i][j+3]->ZwrocStanPola() == 1  && poleGry[i][j+4]->ZwrocStanPola() == 1)
			{

				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+1]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+2]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+3]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+4]->RysujPrzekreslenie(obraz, gdzie, "poziome");

				wygranaKolko = true;
				break;
			}

			else if(poleGry[i][j]->ZwrocStanPola() == 2 && poleGry[i][j+1]->ZwrocStanPola() == 2  
				&& poleGry[i][j+2]->ZwrocStanPola() == 2  && poleGry[i][j+3]->ZwrocStanPola() == 2  && poleGry[i][j+4]->ZwrocStanPola() == 2)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+1]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+2]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+3]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+4]->RysujPrzekreslenie(obraz, gdzie, "poziome");

				wygranaKrzyzyk = true;
				break;
			}

			else if(poleGry[i][j]->ZwrocStanPola() == 3 && poleGry[i][j+1]->ZwrocStanPola() == 3  
				&& poleGry[i][j+2]->ZwrocStanPola() == 3  && poleGry[i][j+3]->ZwrocStanPola() == 3  && poleGry[i][j+4]->ZwrocStanPola() == 3 && trzechGraczy)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+1]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+2]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+3]->RysujPrzekreslenie(obraz, gdzie, "poziome");
				poleGry[i][j+4]->RysujPrzekreslenie(obraz, gdzie, "poziome");

				wygranaPlus = true;
				break;
			}


			//dla pionu
			if(poleGry[i][j]->ZwrocStanPola() == 1 && poleGry[i+1][j]->ZwrocStanPola() == 1  
				&& poleGry[i+2][j]->ZwrocStanPola() == 1  && poleGry[i+3][j]->ZwrocStanPola() == 1  && poleGry[i+4][j]->ZwrocStanPola() == 1)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+1][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+2][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+3][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+4][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				wygranaKolko = true;
				break;
			}

			else if(poleGry[i][j]->ZwrocStanPola() == 2 && poleGry[i+1][j]->ZwrocStanPola() == 2  
				&& poleGry[i+2][j]->ZwrocStanPola() == 2  && poleGry[i+3][j]->ZwrocStanPola() == 2  && poleGry[i+4][j]->ZwrocStanPola() == 2)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+1][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+2][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+3][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+4][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				wygranaKrzyzyk = true;
				break;
			}
			else if(poleGry[i][j]->ZwrocStanPola() == 3 && poleGry[i+1][j]->ZwrocStanPola() == 3  
				&& poleGry[i+2][j]->ZwrocStanPola() == 3  && poleGry[i+3][j]->ZwrocStanPola() == 3  && poleGry[i+4][j]->ZwrocStanPola() == 3 && trzechGraczy)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+1][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+2][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+3][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				poleGry[i+4][j]->RysujPrzekreslenie(obraz, gdzie, "pionowe");
				wygranaPlus = true;
				break;
			}


			//dla skosu prawo
			if(poleGry[i][j]->ZwrocStanPola() == 1 && poleGry[i+1][j+1]->ZwrocStanPola() == 1  
				&& poleGry[i+2][j+2]->ZwrocStanPola() == 1  && poleGry[i+3][j+3]->ZwrocStanPola() == 1  && poleGry[i+4][j+4]->ZwrocStanPola() == 1)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+1][j+1]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+2][j+2]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+3][j+3]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+4][j+4]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");

				wygranaKolko = true;
				break;
			}

			else if(poleGry[i][j]->ZwrocStanPola() == 2 && poleGry[i+1][j+1]->ZwrocStanPola() == 2 
				&& poleGry[i+2][j+2]->ZwrocStanPola() == 2  && poleGry[i+3][j+3]->ZwrocStanPola() == 2  && poleGry[i+4][j+4]->ZwrocStanPola() == 2)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+1][j+1]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+2][j+2]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+3][j+3]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+4][j+4]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");

				wygranaKrzyzyk = true;		
				break;
			}
			else if(poleGry[i][j]->ZwrocStanPola() == 3 && poleGry[i+1][j+1]->ZwrocStanPola() == 3 
				&& poleGry[i+2][j+2]->ZwrocStanPola() == 3  && poleGry[i+3][j+3]->ZwrocStanPola() == 3  && poleGry[i+4][j+4]->ZwrocStanPola() == 3 && trzechGraczy)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+1][j+1]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+2][j+2]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+3][j+3]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");
				poleGry[i+4][j+4]->RysujPrzekreslenie(obraz, gdzie, "ukosne_p");

				wygranaPlus = true;		
				break;
			}

			//dla skosu w lewo
			if(poleGry[i][j]->ZwrocStanPola() == 1 && poleGry[i+1][j-1]->ZwrocStanPola() == 1  
				&& poleGry[i+2][j-2]->ZwrocStanPola() == 1  && poleGry[i+3][j-3]->ZwrocStanPola() == 1  && poleGry[i+4][j-4]->ZwrocStanPola() == 1)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+1][j-1]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+2][j-2]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+3][j-3]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+4][j-4]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");

				wygranaKolko = true;
				break;
			}

			else if(poleGry[i][j]->ZwrocStanPola() == 2 && poleGry[i+1][j-1]->ZwrocStanPola() == 2  
				&& poleGry[i+2][j-2]->ZwrocStanPola() == 2  && poleGry[i+3][j-3]->ZwrocStanPola() == 2  && poleGry[i+4][j-4]->ZwrocStanPola() == 2)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+1][j-1]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+2][j-2]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+3][j-3]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+4][j-4]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");

				wygranaKrzyzyk = true;
				break;
			}
			else if(poleGry[i][j]->ZwrocStanPola() == 3 && poleGry[i+1][j-1]->ZwrocStanPola() == 3  
				&& poleGry[i+2][j-2]->ZwrocStanPola() == 3  && poleGry[i+3][j-3]->ZwrocStanPola() == 3  && poleGry[i+4][j-4]->ZwrocStanPola() == 3 && trzechGraczy)
			{
				poleGry[i][j]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+1][j-1]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+2][j-2]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+3][j-3]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");
				poleGry[i+4][j-4]->RysujPrzekreslenie(obraz, gdzie, "ukosne_l");

				wygranaPlus = true;
				break;
			}
			else if(liczRemis == MAX_X * MAX_Y)
			{
             remis = true;
			 break;
			}



       }
	}
}
