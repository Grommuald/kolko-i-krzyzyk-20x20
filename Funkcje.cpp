#include "Funkcje.h"

bool Kolizja(SDL_Rect A, SDL_Rect B)
{
	//wszytkie strony naszych kwadratow
	int lewaA, lewaB; 
	int prawaA, prawaB; 
	int goraA, goraB;
	int dolA, dolB; 
	//ustawiamy strony w A
	lewaA = A.x; 
	prawaA = A.x + A.w; 
	goraA = A.y; 
	dolA = A.y + A.h; 
	//ustawiamy strony w B
	lewaB = B.x; 
	prawaB = B.x + B.w; 
	goraB = B.y; 
	dolB = B.y + B.h;

	//sprawdzamy czy strony obu prostokatow nie przenikaja sie
	if( dolA <= goraB ) { return false; } 
	if( goraA >= dolB ) { return false; } 
	if( prawaA <= lewaB ) { return false; } 
	if( lewaA >= prawaB ) { return false; } 
	//jezeli nie zachodza zadne kolizje zwroc
	return true; 
}

SDL_Surface *Laduj(string nazwa_pliku)
{
	SDL_Surface* ladowany = NULL;
    //zwracamy zaladowany pozniej ten obrazek(powierzchnie)
    SDL_Surface* optymalizowany = NULL;

	//ladujemy...
	ladowany = IMG_Load(nazwa_pliku.c_str());
	//sprawdzamy czy nic nie poszlo zle...
    if( ladowany != NULL )
    {
        //tworzymy zoptymalizowany obrazek
        optymalizowany = SDL_DisplayFormat( ladowany );
        SDL_FreeSurface( ladowany );
    }
	else
		return false;

	//na koniec zwracamy zoptymalizowana powierzchnie 
	return optymalizowany;
}
void Rysuj(int x, int y, SDL_Surface *co, SDL_Surface *gdzie, SDL_Rect *wycinek)
{
//pomocniczy do umieszczenia obrazka na jakies pozycji
SDL_Rect poz;
poz.x = x;
poz.y = y;

SDL_BlitSurface(co, wycinek, gdzie, &poz);

}
void UstawKluczowanieKoloru(SDL_Surface *dla, Uint8 r, Uint8 g, Uint8 b)
{
		Uint32 colorMask = SDL_MapRGB(dla->format, r, g, b);
		SDL_SetColorKey(dla, SDL_SRCCOLORKEY|SDL_RLEACCEL, colorMask);
}
bool WcisnalLPM(SDL_Event kontrola)
{
  if( kontrola.type == SDL_MOUSEBUTTONDOWN ) 
  { 
	//lewy przycisk
	if( kontrola.button.button == SDL_BUTTON_LEFT ) 
	{ 
	    return true;
	}

  }
}
bool WcisnalPPM(SDL_Event kontrola)
{
   if( kontrola.type == SDL_MOUSEBUTTONDOWN ) 
   { 
	//prawy przycisk
	if( kontrola.button.button == SDL_BUTTON_RIGHT ) 
	{ 
	  return true;
	}

   }

}