#include "Pole.h"

void Pole::RysujPole(SDL_Surface *obraz, SDL_Surface *gdzie)
{
 if(podswietlenie)
 {
	 if(aktywne)
		 wycinek.x = 128;
	 else
		 wycinek.x = 96;
 }
 else
 {
	 if(rodzajPola == 0) //puste
	   wycinek.x = 160;
	 
	 else if(rodzajPola == 1) //dla k�ka
	   wycinek.x = 32;
	 else if(rodzajPola == 2) //dla krzyzyka
	   wycinek.x = 0;
	 else if(rodzajPola == 3) //dla plusa
	   wycinek.x = 64;

 }


	Rysuj(pozycja.x, pozycja.y, obraz, gdzie, &wycinek);
}
bool Pole::RysujPrzekreslenie(SDL_Surface *obraz, SDL_Surface *gdzie, string jakie)
{
	
	 if(jakie == "poziome") //przekreslenie poziome
	   wycinek.x = 288;
	 else if(jakie == "pionowe") //przdekreslenie pionowe
	   wycinek.x = 256;
	 else if(jakie == "ukosne_p") //przdekreslenie ukosne lewe
	   wycinek.x = 224;
	 else if(jakie == "ukosne_l") //przdekreslenie ukosne prawe
	   wycinek.x = 192;
	 else
	 {
		 return false;
	 }

	Rysuj(pozycja.x, pozycja.y, obraz, gdzie, &wycinek);
}