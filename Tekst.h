#pragma once
#ifndef _TEKST_H
#define _TEKST_H

#include "Funkcje.h"

class Tekst
{
public:


	Tekst();
	Tekst(int x, int y, int wielkosc, const char *nazwaCzcionki);
   ~Tekst();


	void ZmienPozycje(int wyznacz_x, int wyznacz_y);
	void ZmienKolor(int wyznacz_r, int wyznacz_g, int wyznacz_b);
	void ZmienWielkosc(int wyznaczWielkosc);

	int  PiszTekst(string tresc, SDL_Surface *gdzie);

	int  PiszTekst(int dana, SDL_Surface *gdzie);
	int  PiszTekst(double dana, SDL_Surface *gdzie);
	int  PiszTekst(float dana, SDL_Surface *gdzie);

private:

	int x;
	int y;

	int wielkosc;

	const char *nazwaCzcionki;

	int r, g, b;

	TTF_Font *czcionka; 
	SDL_Color czcionkaKolor;
	SDL_Surface *czcionkaPow;


};


#endif