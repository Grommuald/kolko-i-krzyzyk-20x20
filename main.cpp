#include "Gra.h"

SDL_Event kontrola;
SDL_Surface *Ekran = NULL;
SDL_Surface *assety = NULL;

Gra gra;

int main(int agrc, char *agrv[])
{
 bool wyjdz = false;

 SDL_Init(SDL_INIT_EVERYTHING);
 SDL_WM_SetCaption( "Kolko i Krzyzyk 2.0", NULL );
 Ekran = SDL_SetVideoMode(1024, 640, 32, SDL_SWSURFACE);

 TTF_Init();
 assety = Laduj("obraz.png");

 gra.OdNowa();
 while(!wyjdz)
 {

	 SDL_FillRect(Ekran, NULL, SDL_MapRGB(Ekran->format, 0,0,0));
	 while( SDL_PollEvent( &kontrola ) ) 
	 { 
		
		 gra.AktualizujWejscie(kontrola);
	     //gdy klikniemy krzy�yk;]
	     if( kontrola.type == SDL_QUIT ) 
	     { 
		   wyjdz = true; 
	     }

		  if( kontrola.type == SDL_KEYDOWN )
            {
                switch( kontrola.key.keysym.sym )
                {
                case SDLK_ESCAPE:
                    wyjdz=true;
                    break;
                }
            }
	
	 }


     gra.RysujWszystko(assety, Ekran);
	 gra.SprawdzPole(assety, Ekran);
	 gra.Wygrana(Ekran);

	 SDL_Flip(Ekran);

 }
 SDL_FreeSurface(Ekran);
 SDL_FreeSurface(assety);

 TTF_Quit();
 SDL_Quit();


	return 0;
}