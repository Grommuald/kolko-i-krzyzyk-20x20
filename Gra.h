#pragma once
#include "Pole.h"
#include "Tekst.h"
#include "Funkcje.h"
#include "Przycisk.h"


const int MAX_X = 20;
const int MAX_Y = 20;

class Gra
{
public:
	Gra(void)
	{
		//uzupe�niamy pole zerami
		for(int i = 0; i < MAX_X; ++i)
		{
			for(int j = 0; j < MAX_Y; ++j)
			{
				poleGry[i][j] = new Pole(0, j*32, i*32);
			}
		}


		restart = new Przycisk("Restart", 650, 600, false);

		dwochGraczy = true;
		trzechGraczy = false;

		pytajIloscGraczy = true;
		koniecImprezy = true;
		ktoZaczyna = "";
	}
	~Gra(void){}

	void SprawdzPole(SDL_Surface *obraz, SDL_Surface *gdzie); //sprawdza stan pola i zwraca true gdy gra toczy sie dalej

	bool Wygrana(SDL_Surface *gdzieRysowac);
	void MenuGlowne();
	void AktualizujWejscie(SDL_Event kontrola);
	void RysujWszystko(SDL_Surface *obrazek, SDL_Surface *gdzie);
	void OdNowa();
	void LosujGracza();//losujemy kto zaczyna 
	void ZmienTure()
	{
	  if(trzechGraczy)
	  {
		aktualnaTura++;
		if(aktualnaTura > 3) aktualnaTura = 1;
		cout << aktualnaTura << endl;

	  }
	  else if(dwochGraczy)
	  {
		aktualnaTura++;
		if(aktualnaTura > 2) aktualnaTura = 1;
		cout << aktualnaTura << endl;
	  }
	}

private:

	bool wygranaKrzyzyk;
	bool wygranaKolko;
	bool wygranaPlus;
	bool remis;

	bool dwochGraczy;
	bool trzechGraczy;

	bool pytajIloscGraczy;
	bool koniecImprezy;

	bool wyswietlPytanie;

	string ktoZaczyna;

	int liczRemis; //je�eli pole jest zaj�te
	int aktualnaTura; //kto ma teraz ruch(1 || 2)

	string napisy[2];
	Pole    *poleGry[MAX_X][MAX_Y];
	Przycisk *restart;


};
