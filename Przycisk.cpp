#include "Przycisk.h"

Przycisk::Przycisk()
{
	    pozycja.x= 0;
		pozycja.y= 0;

		pozycja.w = wycinek.w = 80;
		pozycja.h = wycinek.h = 32;

		TekstPrzycisku = "noname";

		wcisniety = false;
		podswietlony = false;
		przyciskOdznaczajacy = false;

}
//==========================================================================================================
//==========================================================================================================
void Przycisk::AktualizujWejscie(SDL_Event kontrola)
{
    //pozycja myszy
	int x = 0, y = 0; 
	//pobieramy pozycje myszy
	SDL_GetMouseState( &x, &y ); 

	SDL_Rect mysz = {x, y, 1, 1};

   if( Kolizja(mysz, pozycja) == true)
   {
    //lontrolujemy mysz
	 if( kontrola.type == SDL_MOUSEBUTTONDOWN ) 
      { 
	     //lewy przycisk
	     if( kontrola.button.button == SDL_BUTTON_LEFT ) 
	     { 
          wcisniety = true; 
		  
		 }
	 }

   }
   else
   {
    
    if(zostawPodswietlenie && wcisniety ==true)
	{
		podswietlony = true;
	}
	else
	{
		podswietlony = false;
		wcisniety = false;
	}

   }



}
//==========================================================================================================
//==========================================================================================================
void Przycisk::RysujPrzycisk(SDL_Surface *obraz, SDL_Surface *gdzie, int r, int g, int b)
{

 //rysujemy odpowiedni stan naszego przycisku

  if(!przyciskOdznaczajacy)
  {
	Tekst Przycisk(pozycja.x + 10, pozycja.y + wycinek.h/3, 15, "morgue.ttf");
    Przycisk.ZmienKolor(r, g, b);
  
    if(podswietlony)
	{
    wycinek.x = 320;
	wycinek.y = 0;
	}
	else
	{
	wycinek.x = 400;
	wycinek.y = 0;
	}

	Rysuj(pozycja.x, pozycja.y, obraz, gdzie, &wycinek); //najpierw przycisk 
	Przycisk.PiszTekst(TekstPrzycisku, gdzie);
  }
  else
  {
    Tekst Przycisk(pozycja.x - 10, pozycja.y - 10, 10, "morgue.ttf");
    Przycisk.ZmienKolor(r, g, b);

	if(podswietlony)
	{
    wycinek.x = 512;
	wycinek.y = 0;
	}
	else
	{
	wycinek.x = 480;
	wycinek.y = 0;
	}

	Rysuj(pozycja.x, pozycja.y, obraz, gdzie, &wycinek); //najpierw przycisk 
	Przycisk.PiszTekst(TekstPrzycisku, gdzie);
  }





}