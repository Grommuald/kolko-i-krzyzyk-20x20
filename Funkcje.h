#ifndef _FUNKCJE_H
#define _FUNKCJE_H

#include "Naglowki.h"

void         Rysuj(int x, int y, SDL_Surface *co, SDL_Surface *gdzie, SDL_Rect *wycinek = NULL); //przenies obraz na ekran
SDL_Surface *Laduj(string nazwa_pliku);  //laduj obraz do pamieci
bool         Kolizja(SDL_Rect A, SDL_Rect B);   //sprawdz czy dwa prostokaty nie koliduja sie ze soba
void         UstawKluczowanieKoloru(SDL_Surface *dla, Uint8 r, Uint8 g, Uint8 b);
bool         WcisnalLPM(SDL_Event kontrola);
bool         WcisnalPPM(SDL_Event kontrola);

#endif