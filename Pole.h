#pragma once
#include "Funkcje.h"

class Pole
{
public:
	Pole(int _rodzajPola, int x, int y)
	{
		rodzajPola = _rodzajPola;
		pozycja.x = x;
		pozycja.y = y;
		wycinek.w = pozycja.w = 32;
		wycinek.h = pozycja.h = 32;

		wycinek.x = 160;
		wycinek.y = 0;

		aktywne = true;
		
	}
	Pole(){rodzajPola = 0;}
   ~Pole(){}

    bool ZwrocAktywnosc(){return aktywne;}

	void ZmienStanPola(int zmien){rodzajPola = zmien;}
    int  ZwrocStanPola(){return rodzajPola;} //zwracamy co si� dzieje na polu 
	void RysujPole(SDL_Surface *obraz, SDL_Surface *gdzie);
	bool RysujPrzekreslenie(SDL_Surface *obraz, SDL_Surface *gdzie, string jakie);
	SDL_Rect ZwrocPole(){return pozycja;}

	bool aktywne;
	bool podswietlenie;

private:
	int rodzajPola; //gdy 0 to puste, gdy 1 to kolko, gdy 2 to krzyzyk
	

	SDL_Rect pozycja;
	SDL_Rect wycinek;
};
